# Practica 5 - Spark Structured Streaming - Stream-stream join and sessionization

## Ejercicio CallDataSummary

### Objetivo

El objetivo de este ejercicio es escribir una aplicación de streaming que procese los eventos de red desde el topic 
`streaming.network.records`, para generar datos agregados de llamada en un topic Kafka de salida. Los registros de salida
deben ir codificados en formato JSON e incluir los siguientes atributos:

|Field|Type|Description|
|---|---|---|
|callId|String|Identificador de la llamada|
|tsIni|Timestamp|Fecha y hora del comienzo de la llamada|
|msisdnOrig|String|Numero de telefono originante|
|msisdnTerm|String|Numero de telefono terminante|
|conversationTime| Int |Duracion de la llamada en segundos


El esquema de los eventos de red, codificados en formato json, es el mismo que el descrito en la Practica 4

Un ejemplo real de registro en el topic:
```bash
{"timestamp": "2019-04-09 16:29:11.795", "msisdn": "600000063", "call_id": "581d2851-5528-4baa-9316-8432d04d736f", "outgoing_call": false, "cell": "cell-00691", "event_type": 0, "conversation_time": 0}
```

El pipeline de transformación se codificará empleando tanto la funcionalidad de Stream-Stream joins como la de sesionización (`GroupStateAPI`)

La referencia de estas funciones se puede encontrar en el siguiente enlace de la documentación oficial:

https://spark.apache.org/docs/2.3.1/structured-streaming-programming-guide.html#stream-stream-joins
https://spark.apache.org/docs/2.3.1/api/scala/index.html#org.apache.spark.sql.streaming.GroupState

### Compilación y empaquetado

Para compilar y empaquetar la aplicación, ejecutar desde este mismo directorio el siguiente comando:

```bash
mvn clean package
```

También se puede ejecutar la tarea Maven desde el IDE (vista Maven -> Lifecycle -> package)

### Ejecución

El proyecto está configurado para generar un _fat jar_ ejecutable bajo el directorio `target`. Para ejecutarlo, copiarlo 
al nodo edge01 y utilizar `spark2-submit` de la siguiente manera:

```bash
scp target/spark-structured-advanced-0.1.0-SNAPSHOT.jar edge:

spark2-submit --master yarn --num-executors 2 --executor-cores 2 --executor-memory 1G --conf spark.sql.shuffle.partitions=10 \
--packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0  --class edu.comillas.mbd.streaming.spark.practices.CallDataSummary \
spark-structured-advanced-0.1.0-SNAPSHOT.jar
```
