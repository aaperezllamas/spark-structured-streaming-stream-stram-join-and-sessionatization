
import java.sql.Timestamp
import org.apache.log4j.Logger
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.{GroupState, GroupStateTimeout, OutputMode, Trigger}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Dataset, SparkSession}

case class NetworkRecord(timestamp: Timestamp,
                         msisdn: String,
                         callId: String,
                         outgoingCall: Boolean,
                         cell: String,
                         eventType: Int,
                         conversationTime: Int)

case class Cell(cell_id: String,
                city: String,
                province: String)


case class CallData(callId: String,
                    tsIni: java.sql.Timestamp,
                    msisdnOrig: String,
                    msisdnTerm: String,
                    conversationTime: Int)

case class KafkaRecord(key: String, value: CallData)


object CallDataSummary {

  // TODO set your user name here
  val user: String = "aalvarez"

  // TODO change this if needed
  val destinationTopic: String = s"streaming.alumnos.$user.network"

  val InputTopic = "streaming.network.records"

  private val logger = Logger.getLogger(getClass.getName)

  private val spark = SparkSession.builder()
    .appName(s"CallDataSummary - $user")
    .master("yarn")
    .getOrCreate()

  // this is needed to use symbols as column names
  import spark.implicits._


  val networkRecordSchema = StructType(
    List(
      StructField("timestamp", TimestampType, nullable = false),
      StructField("msisdn", StringType, nullable = false),
      StructField("call_id", StringType, nullable = false),
      StructField("outgoing_call", BooleanType, nullable = false),
      StructField("cell", StringType, nullable = false),
      StructField("event_type", IntegerType, nullable = false),
      StructField("conversation_time", IntegerType, nullable = false)
    )
  )

  /**
    * Returns a streaming dataframe from the specified topic
    * @param kafkaTopic
    * @return
    */
  def getInputTableFromTopic(kafkaTopic: String): Dataset[NetworkRecord] = {
    spark.readStream
      .format("kafka")
      .option("subscribe", kafkaTopic)
      .option("kafka.bootstrap.servers", "master01.bigdata.alumnos.upcont.es:9092")
      .option("startingOffsets", "latest")
      .load()
      .select('value cast StringType)
      .select(from_json('value, networkRecordSchema) as 'network_record_struct)
      .select(to_timestamp('network_record_struct.getField("timestamp"), "yyyy-MM-dd HH:mm:ss.SSS") as 'timestamp,
        'network_record_struct.getField("msisdn") as 'msisdn,
        'network_record_struct.getField("call_id") as 'callId,
        'network_record_struct.getField("outgoing_call") as 'outgoingCall,
        'network_record_struct.getField("cell") as 'cell,
        'network_record_struct.getField("event_type") cast IntegerType as 'eventType,
        'network_record_struct.getField("conversation_time") cast IntegerType as 'conversationTime)
      .as[NetworkRecord]
  }

  def getCaseClass(elem:NetworkRecord, s:CallData = null): CallData = {

    if (s != null) {
      if(elem.outgoingCall){
        return CallData(elem.callId, elem.timestamp, elem.msisdn, "", elem.conversationTime)
      }else{
        return CallData(elem.callId, elem.timestamp, "", elem.msisdn, elem.conversationTime)
      }
    }else {

      if(elem.outgoingCall){
        return CallData(elem.callId, elem.timestamp, elem.msisdn, s.msisdnTerm, elem.conversationTime)
      }else{
        return CallData(elem.callId, elem.timestamp, s.msisdnOrig, elem.msisdn, elem.conversationTime)
      }
    }

  }

  def callsMappingFunction(key:String, value:Iterator[NetworkRecord], state:GroupState[CallData]):Iterator[CallData] = {

    if (state.exists){
      for (elem <- value){
        if (elem.eventType == 3){
          val lastState = getCaseClass(elem, state.get)
          state.remove()
          return Iterator[CallData](lastState)
        }else{
          state.update(getCaseClass(elem, state.get))
        }
      }
      return Iterator[CallData](state.get)
    }else{
      for (elem <- value){
        if (!state.exists){
          if (elem.eventType == 1){
            state.update(getCaseClass(elem))
          }
        }else{
          if (elem.eventType == 1){
            state.update(getCaseClass(elem, state.get))
          } else if (elem.eventType == 3){
            val lastState = getCaseClass(elem, state.get)
            state.remove()
            return Iterator[CallData](lastState)
          }
        }
      }
      if (state.exists) Iterator[CallData](state.get) else Iterator[CallData]()
    }

  }


  def main(args: Array[String]): Unit = {
    logger.info(s"Creating Spark Structured Streaming input table from topic $InputTopic")

    val inputTable: Dataset[NetworkRecord] = getInputTableFromTopic(InputTopic)

    // TODO implement this
    val resultTable: Dataset[CallData] = inputTable
        .groupByKey(r => r.callId)
        .flatMapGroupsWithState(OutputMode.Append(), GroupStateTimeout.NoTimeout())(callsMappingFunction)

    val streamingQuery = resultTable
      .map(callData => KafkaRecord(callData.msisdnOrig, callData))
      .select('key, to_json('value) as 'value)
      .writeStream
      .outputMode(OutputMode.Append)
      .format("kafka")
      .option("kafka.bootstrap.servers", "master01.bigdata.alumnos.upcont.es:9092")
      .option("topic", destinationTopic)
      .option("checkpointLocation", s"/tmp/spark/streaming/checkpoint/$user/advanced")
      .trigger(Trigger.ProcessingTime("5 seconds"))
      .start()

    logger.info(s"Streaming query ${streamingQuery.name} started!")
    streamingQuery.awaitTermination()
  }
}